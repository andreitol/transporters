window.onload = function () {

  // the appearance of the shadow of the menu when scrolling
  $(window).scroll(function () {
    if ($(window).scrollTop() > 0) {
      $('.menu').addClass('shadow-scroll');
    } else {
      $('.menu').removeClass('shadow-scroll');
    }
  });

  // open/close user-block
  $('.menu-toggle').on('click', function () {
    // debugger;
    $('.user-block').addClass('open');
    $('.profile').addClass('close');
  });
  $('.profile-close').on('click', function () {
    // debugger;
    $('.user-block').removeClass('open');
    // $('.user-block').addClass('close');
    $('.profile').removeClass('close');
  });

  // open/close user-menu
  $('.user-toggle').on('click', function () {
    $('.user-menu').slideToggle(1000, function () {
      $('.user-menu').stop(true);
    });
    $('.fa-angle-down').toggleClass('flip');
  });

  // send form
  $('form').submit(function (e) { // проверка на пустоту заполненных полей. Атрибут html5 — required не подходит (не поддерживается Safari)
    // e.preventDefault();
    if (document.form.name.value == '' || document.form.phone.value == '') {
      valid = false;
      return valid;
    }
    $.ajax({
      type: "POST",
      url: "url",
      data: $(this).serialize()
    }).done(function () {
      $('.overlay-show').fadeIn();
      $(this).find('input').val('');
      $('form').trigger('reset');
    });
    return false;
    // e.preventDefault();
  });

  // Close pop-up sucess
  $('.overlay-show').click(function () { // по клику на крестик
    $('.overlay-show').fadeOut();
  });

  // password length check
  if (password < 5) {
    $('.errow-password').fadeIn();
  }
}

