var gulp      = require('gulp'),
	sass        = require('gulp-sass'),
	plumber     = require('gulp-plumber'), //Подключаем plumber пакет
	notify 			= require('gulp-notify'),
	browserSync = require('browser-sync'),
	pug 				= require('gulp-pug'), //Подключаем pug пакет
	autoprefixer = require('gulp-autoprefixer'),
	sourcemaps  = require('gulp-sourcemaps');

	// Images
var imagemin = require('gulp-imagemin'),
		pngquant = require('imagemin-pngquant'),
		jpegtran = require('imagemin-jpegtran'),
		jpegrecompress = require('imagemin-jpeg-recompress'),
		svgo = require('imagemin-svgo');

// HTML, CSS, JS
var htmlclean = require('gulp-htmlclean'),
		htmlbeautify = require('gulp-html-beautify'),
		cleancss     = require('gulp-clean-css'),
		concat       = require('gulp-concat'),
		rename       = require('gulp-rename');



gulp.task('code', function () {
	return gulp.src('app/*.html')
		.pipe(browserSync.reload({ stream: true }))
});


gulp.task('styles', function () { // Создаем задачу "styles"
	return gulp.src('./app/sass/styles.sass', { sourcemaps: true }) // Берем источник
		.pipe(plumber({
			errorHandler: notify.onError(function (err) { // Проверяем источник на ошибки
				return {
					title: 'Styles',
					message: err.message
				}
			})
		}))
		.pipe(sourcemaps.init())
		.pipe(sass()) // Преобразуем Scss в CSS посредством gulp-sass
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('app/css/')) // Выгружаем результата в папку app / css
		.pipe(browserSync.stream()); // Обновляем CSS на странице при изменении
});

gulp.task('pug', function () { // Создаем задачу "pug"
	return gulp.src('app/pug/*.pug') // Берем источник
		.pipe(plumber({ // Проверяем источник на ошибки
			errorHandler: notify.onError(function (err) {
				return {
					title: 'Pug',
					message: err.message
				}
			})
		}))
		.pipe(pug({ // Преобразуем Pug в html посредством gulp-pug
			pretty: true
		}))
		.pipe(gulp.dest('app/')) // Выгружаем результат в папку app
		.on('end', browserSync.reload); // страница обновится только после полного выполнения задачи pug
	/* 		.pipe(browserSync.reload({ stream: true })); // Обновляем CSS на странице при изменении */
});


// следим за файлами
gulp.task('watch', function () {
	gulp.watch('./app/pug/**/*.pug', gulp.parallel('pug'));
	gulp.watch('./app/sass/**/*.+(sass|scss)', gulp.parallel('styles'));
	gulp.watch('./app/css/*.css', browserSync.reload);
	gulp.watch('./app/img/*.+(jpg|png|svg)', browserSync.reload);
	gulp.watch('./app/js/*.js', browserSync.reload);
	// gulp.watch('./app/fonts/*', browserSync.reload);
});

gulp.task('server', function () {
	browserSync.init({
		server: {
			baseDir: "./app", // Директория для сервера
		},
		// browser: "firefox",
		notify: false // Отключаем уведомления
	});
	browserSync.watch('app/*', browserSync.reload)
	// browserSync.watch('app/*').on("change", reload);
});

// gulp.task('default', gulp.series(gulp.parallel('pug', 'sass'),
// 	gulp.parallel('watch', 'server')
// ));
gulp.task('default', gulp.parallel('pug', 'styles', 'watch', 'server'));


// === Tasks for production === //

gulp.task('prod:html', function () {
	return gulp.src('app/pug/index.pug')
	.pipe(pug({ pretty: true}))
	.pipe(htmlbeautify({indentSize: 2}))
	.pipe(gulp.dest('prod/'))
});

// Compress img
gulp.task('mini:img', function() {
	return gulp.src(['app/img/**/*.+(jpg|png|svg)','!app/img/favicon/*.*'])
	.pipe(imagemin([
		// imagemin.gifsicle({interlaced: true}),
		jpegtran(),
		jpegrecompress({
			interlaced: true,
			progressive: true,
			max: 80,
			min: 70
		}),
		pngquant({quality: [0.6, 0.8]}),
		svgo({plugins: [
			{removeViewBox: false},
			{cleanupIDs: false}
		]})
	]))
	.pipe(gulp.dest('prod/img'))
});

// Just copy
gulp.task('copy:favicon', function() {
	return gulp.src('app/img/favicon/*.*')
		.pipe(gulp.dest('prod/img/favicon/'));
});

gulp.task('copy:js', function() {
	return gulp.src('app/js/*.js')
		.pipe(gulp.dest('prod/js'));
});

// CSS
gulp.task('copy:css', function () {
	return gulp.src('app/css/*.css')
	.pipe(gulp.dest('prod/css/'))
});

// JS minification
// gulp.task('mini:js', function () {
// 	return gulp.src([
// 		'app/js/*.js','!app/js/jquery-3.4.1.slim.min.js','!app/js/moment.min.js','app/js/scripts.js'])
// 		.pipe(concat('all.min.js'))
// 		.pipe(uglify()) // Mifify js (opt.)
// 		.pipe(gulp.dest('prod/js'))
// });

//CSS minification
gulp.task('mini:css', function () {
	return gulp.src(['app/css/sanitize.css','app/css/_reset.css','app/css/countdown-clock.css','app/css/styles.css'])
	.pipe(autoprefixer({browsers: ['last 3 versions']}))
	.pipe(concat('all.css'))
	.pipe(cleancss({
		level: { 1: { all: false, removeDuplicateRules: true, tidySelectors :  true } },
		format: 'keep-breaks'
	}))
	.pipe(rename({ suffix: '.min', prefix : '' }))
	.pipe(gulp.dest('prod/css/'))
});

gulp.task('prod', gulp.parallel(
	'prod:html',
	'mini:img',
	'copy:favicon',
	// 'copy:logo',
	'copy:js',
	// 'mini:js',
	// 'mini:css',
	'copy:css'
	));
